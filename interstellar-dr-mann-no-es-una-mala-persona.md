TecnoCinFicción Interstellar

# "El Dr. Mann no es una mala persona"
(Una interpretación complementaria)

De Jacinto Dávila

"El Dr. Mann no es una mala persona. Sus credenciales académicas son impecables. 
Fue el investigador pionero  de la determinación de viabilidad de una colonia 
en un planeta distante. Experto en evaluación de factibilidad, siempre fue una
persona amigable, aunque vivía solo y no se le conoció familia consanguínea. Por
eso a nadie sorprendió que frente al proyecto de enviar exploradores a través 
del agujero de gusano cerca de Saturno, él se presentara com primer candidato
a confirmar una unidad de exploración de una sola persona. La misión sería 
instalar módulos de observación para recoger datos de evaluación de viabilidad
para establecer una colonia en el planeta visitado. Eran tan apreciados sus 
logros y esfuerzos que se le permitió escoger a cual de los planetas, recién
identificados, iría. Quizás esas décadas en casi absoluta soledad en un planeta
hostíl puedan quebrar al espiritu más solidario. Quizás su motivación principal
fue siempre su propia salvación. Lo cierto es que, al comprobar que el suyo era
un planeta hostíl a la vida, el Dr Mann decidió mentir. Transmitió datos falsos
sobre su planeta para persuadir a la Tierra de enviar una sonda a desplegar una
colonia allí, pero solo pensando en usar esa nave para su propio rescate. Tuvo
mucho tiempo para planear esa farsa. Incluso pudo desmotar al robot quien debíó
oponerse a su plan en algún momento. Y lo convirtió en un dispositivo de auto-
destrucción si alguien trataba de rescatar su memoria. Siempre supo que el plan
A no serviría. También llegó a creer que "ellos", esos seres que parecían hacer
cosas para ayudar a los terrícolas a huir de su agonizante planeta, eran los
mismos humanos evolucionados y enviando ayuda desde un futuro distante. No había
ningun Dios juzgando sus acciones fatales. El Dr. Mann no era una mala persona.
Todo eso fue solamente el resultado calculado de lo que podemos ser. Gracias a
sus acciones, conocimos la singularidad y encontramos una solución. Contábamos
con su ira destructiva. *Rage, rage against the dying of the light. Don't go
gentle into that good night.*"