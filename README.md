# TecnoCinFicción 

Este es un repositorio de textos asociados con producciones cinematográficas 
y otras expresiones artísticas. 

# Spoiler Alert - Alerta de soplones

Estos contenidos **podrían revelar el desenlace** de algunas películas de **buen cine**
global. 

Por favor, no los revise si no desea que le cuenten el final de alguna
producción que Ud quiera disfrutar apropiadamente. 

