
# ¿Quién le dice idiota a un héroe?
## Sobre la película los 3 idiotas (Spoiler Alert)

Jacinto Dávila

Descartemos las respuestas triviales. Lo haría quien no haya entendido que quien parece un idiota es realmente un héroe.  
No lo haría quien sabe que se trata de un héroe, aún si se lo puede calificar también de idiota. Los héroes merecen respeto especial. 
Lo haría, sin duda, quien no crea que realmente es un héroe. Pero podemos decir casi con certeza que no lo haría, en serio, quien esté plenamente 
convencido de que se trata de un auténtico héroe. 

La película los tres idiotas cuenta sobre la vida en la Universidad de tres estudiantes de ingeniería que termina, lamento decirlo (tan pronto), bien. 
Cada uno con una carrera exitosa y una vida feliz después de graduarse. Esa suele ser la definición de éxito universitario. Así que son héroes, suponiendo
que un héroe es quienquiera que tiene éxito (frente a las adversidades que resuelve a propósito y no por mera suerte). 
Los productores de la película, sin embargo, insisten en llamarlos idiotas en el título de todo el proyecto cinematográfico. ¿Por qué?

Están jugando a la ironía [1]. Es un juego complejo porque la película presenta, a través de dos de los personajes principales (por cierto, los
antihéroes), una definición alternativa de heroismo: un héroe es quien llega primero en la vida, entendiendo esta como una carrera hacia la riqueza material. 
Una carrera, a primera vista, por el conocimiento, pero en realidad, por el reconocimiento de que uno parece ser el mas inteligente y por lo tanto, merece ser más rico. 
Al final de la historia, de hecho, se sugiere que ser más rico es la indicación definitiva de que se es más inteligente. 

El juego que nos proponen es medio tonto: apreciar las respuestas diversas que nos ofrecen los tres ante esa declaración absoluta de que el héroe, es decir,
el que definitivamente no es idiota, es quien triunfa de acuerdo a la definición de los antihéroes. Es un juego tonto porque ¿quien puede simpatizar con ese tiránico
director y con el odiosísimo y neurótico (competitivo, dirían algunos) compañero?. 

Desde luego, no se trata de simpatizar. Se trata de establecer quién es el "mas excelente" si permiten el abuso con las dos palabras yuxtapuestas. La excelencia no puede ser 
medida en términos de simpatía, aunque a todos nos guste simpatizar con la excelencia. Hay que medirla "objetivamente" sobre algunas variables que todas y todos puedan
verificar. Por ejemplo, el salario que se termina ganando, el valor de mercado de la casa o el carro que uno posee  o de los regalos que  uno obsequia. Pero también el número de patentes
que posee. O de libros que ha vendido. O de visitas a su blog. O de descargas de su videojuego. O de likes. O de RTs. O de bitcoins (lo siento Andrés, tenía que decirlo). 
Todas esas son indicaciones categóricas de que uno ha acumulado algo valioso y que, por tanto es excelente. Y como, además, son medidas objetivas, se les puede convertir fácilmente 
en unidades monetarias (las bitcoins ya lo son. Por eso tenía que decirlo Andrés). 

Así que, quizás, la propuesta de los antihéroes no es tan repulsiva. Aquí estamos todos persiguiendo calificaciones o diplomas o alguna retribución por lo que aprendemos
a modo de prueba, incluso para uno mismo, de que lo hemos hecho bien (excelente). 

Raju Rastogi, Farhan Qureshi y Ranchhoddas Chanchad (Phunsukh Wangdu) son unos idiotas por atreverse a creer que se podía ser feliz solo por hacer y aprender a hacer lo que a uno le 
gusta hacer. Y no fue fácil para ninguno de ellos alcanzar la "idiotez".  Para dejar de ser idiotas tendrían que optar por lo más rentable. 

Preguntémonos ahora ¿cuán idiota querrá ser Pía? (recuerden que es una doctora en medicina)

[1] https://es.wikipedia.org/wiki/Iron%C3%ADa