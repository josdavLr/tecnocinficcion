Me gusta mucho este ejercicio de análisis de cada personaje. Permite ver cómo las personas nos convertimos en las causas de ciertos efectos y no otros. Es muy difícil de hacer, así que es comprensible que no nos salga bien. 

Creo que te juzgas muy duro al Dr. Brand. Un viejo científico que llega, con toda su ciencia, a creer que ese plan A no funcionará (porque hay algo que no se puede hacer) y que solo queda sacrificarse por plan B. Tuvo que mentir y eso, ciertamente califica como hipocresía, pero es la hipocresía de un padre que sabe que debe disciplinar a sus hijos para impedir que se metan en más problemas. No es la hipocresía de Dr. Mann quien decide hacer todo lo que puede salvarse a sí mismo sin importar las consecuencias. Entiendo que no es fácil separar, con palabras, las dos hipocresías. Pero estoy casi seguro que un tribunal NO condenaría al Dr. Brand por lo que hizo y Sí lo haría con Dr. Mann. Necesitamos a algunos hipócritas. ¿Pero a cuáles?. 

Creo que te vuelves a equivocar, esta vez quizás por culpa de un prejuicio social, acerca del hermano de Murph. Él desde niño da muestras de no tener inclinación por otra cosa que no sea la agricultura. Nos lo dicen en la película, en esa parodia de esos odiosos ejercicios académicos que pretenden decir para qué sirve cada persona. Pero yo no lo llamaría de mente cerrada. Es sólo que no sabe de ese tema tanto como ella y, además, tiene un trauma de la separación de su padre que lo lleva a querer mantener a su familia cerca, sin importar qué pase. ¿Qué tal esta explicación?. En todo caso, asumir que alguien tiene la mente cerrada es inútil. Solo nos conduce a dejarlos morir. 

Tus últimos dos párrafos nos llevan a otros escenarios donde la caracterización de las personas y del cómo nos convertimos en las causas de las cosas es todavía más difícil. Creo que había que "eliminar" a Doyle quien, como muestra esa conducta que describes, estaba enamorado de la Dra Brand. De haberse salvado, habría complicado la trama con Coop. Me pregunto si habrá alguna fuerza en el Universo calculando esos dramas. 

Yo también quiero ver la secuela que, al parecer, ya están filmando. 



